﻿namespace FirstModel
{
    partial class View
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.coordinatesPlotView = new OxyPlot.WindowsForms.PlotView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.xStartInput = new System.Windows.Forms.NumericUpDown();
            this.zStartInput = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.zEndInput = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.xEndInput = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.drawButton = new System.Windows.Forms.Button();
            this.rollPlotView = new OxyPlot.WindowsForms.PlotView();
            this.accPlotView = new OxyPlot.WindowsForms.PlotView();
            this.label5 = new System.Windows.Forms.Label();
            this.yawInput = new System.Windows.Forms.NumericUpDown();
            this.hEndInput = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.hStartInput = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.hPlotView = new OxyPlot.WindowsForms.PlotView();
            ((System.ComponentModel.ISupportInitialize)(this.xStartInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zStartInput)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zEndInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xEndInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yawInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hEndInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hStartInput)).BeginInit();
            this.SuspendLayout();
            // 
            // coordinatesPlotView
            // 
            this.coordinatesPlotView.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.coordinatesPlotView.Location = new System.Drawing.Point(12, 182);
            this.coordinatesPlotView.Name = "coordinatesPlotView";
            this.coordinatesPlotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.coordinatesPlotView.Size = new System.Drawing.Size(482, 398);
            this.coordinatesPlotView.TabIndex = 0;
            this.coordinatesPlotView.Text = "Координаты";
            this.coordinatesPlotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.coordinatesPlotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.coordinatesPlotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "X:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Z:";
            // 
            // xStartInput
            // 
            this.xStartInput.DecimalPlaces = 2;
            this.xStartInput.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.xStartInput.Location = new System.Drawing.Point(34, 71);
            this.xStartInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.xStartInput.Name = "xStartInput";
            this.xStartInput.Size = new System.Drawing.Size(107, 22);
            this.xStartInput.TabIndex = 3;
            // 
            // zStartInput
            // 
            this.zStartInput.DecimalPlaces = 2;
            this.zStartInput.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.zStartInput.Location = new System.Drawing.Point(34, 99);
            this.zStartInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.zStartInput.Name = "zStartInput";
            this.zStartInput.Size = new System.Drawing.Size(107, 22);
            this.zStartInput.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Info;
            this.groupBox1.Controls.Add(this.hStartInput);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.zStartInput);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.xStartInput);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(165, 164);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Введите координаты точки начала:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Info;
            this.groupBox2.Controls.Add(this.hEndInput);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.zEndInput);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.xEndInput);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(183, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(165, 164);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Введите координаты точки конца:";
            // 
            // zEndInput
            // 
            this.zEndInput.DecimalPlaces = 2;
            this.zEndInput.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.zEndInput.Location = new System.Drawing.Point(37, 99);
            this.zEndInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.zEndInput.Name = "zEndInput";
            this.zEndInput.Size = new System.Drawing.Size(107, 22);
            this.zEndInput.TabIndex = 4;
            this.zEndInput.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "X:";
            // 
            // xEndInput
            // 
            this.xEndInput.DecimalPlaces = 2;
            this.xEndInput.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.xEndInput.Location = new System.Drawing.Point(37, 71);
            this.xEndInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.xEndInput.Name = "xEndInput";
            this.xEndInput.Size = new System.Drawing.Size(107, 22);
            this.xEndInput.TabIndex = 3;
            this.xEndInput.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Z:";
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(376, 132);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(92, 43);
            this.drawButton.TabIndex = 7;
            this.drawButton.Text = "Построить маршрут";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // rollPlotView
            // 
            this.rollPlotView.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rollPlotView.Location = new System.Drawing.Point(516, 12);
            this.rollPlotView.Name = "rollPlotView";
            this.rollPlotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.rollPlotView.Size = new System.Drawing.Size(420, 227);
            this.rollPlotView.TabIndex = 8;
            this.rollPlotView.Text = "Координаты";
            this.rollPlotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.rollPlotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.rollPlotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // accPlotView
            // 
            this.accPlotView.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.accPlotView.Location = new System.Drawing.Point(516, 256);
            this.accPlotView.Name = "accPlotView";
            this.accPlotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.accPlotView.Size = new System.Drawing.Size(420, 244);
            this.accPlotView.TabIndex = 9;
            this.accPlotView.Text = "Координаты";
            this.accPlotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.accPlotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.accPlotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(354, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Стартовый угол курса:";
            // 
            // yawInput
            // 
            this.yawInput.DecimalPlaces = 5;
            this.yawInput.Location = new System.Drawing.Point(357, 57);
            this.yawInput.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yawInput.Name = "yawInput";
            this.yawInput.Size = new System.Drawing.Size(107, 22);
            this.yawInput.TabIndex = 11;
            // 
            // hEndInput
            // 
            this.hEndInput.DecimalPlaces = 2;
            this.hEndInput.Location = new System.Drawing.Point(37, 127);
            this.hEndInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.hEndInput.Name = "hEndInput";
            this.hEndInput.Size = new System.Drawing.Size(107, 22);
            this.hEndInput.TabIndex = 13;
            this.hEndInput.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "H:";
            // 
            // hStartInput
            // 
            this.hStartInput.DecimalPlaces = 2;
            this.hStartInput.Location = new System.Drawing.Point(34, 125);
            this.hStartInput.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.hStartInput.Name = "hStartInput";
            this.hStartInput.Size = new System.Drawing.Size(107, 22);
            this.hStartInput.TabIndex = 15;
            this.hStartInput.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "H:";
            // 
            // hPlotView
            // 
            this.hPlotView.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.hPlotView.Location = new System.Drawing.Point(959, 12);
            this.hPlotView.Name = "hPlotView";
            this.hPlotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.hPlotView.Size = new System.Drawing.Size(419, 227);
            this.hPlotView.TabIndex = 12;
            this.hPlotView.Text = "Координаты";
            this.hPlotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.hPlotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.hPlotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1390, 626);
            this.Controls.Add(this.hPlotView);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.yawInput);
            this.Controls.Add(this.accPlotView);
            this.Controls.Add(this.rollPlotView);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.coordinatesPlotView);
            this.Name = "View";
            this.Text = "Простейшее моделирование полета ракеты";
            ((System.ComponentModel.ISupportInitialize)(this.xStartInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zStartInput)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zEndInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xEndInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yawInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hEndInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hStartInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private OxyPlot.WindowsForms.PlotView coordinatesPlotView;

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown xStartInput;
        private System.Windows.Forms.NumericUpDown zStartInput;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown zEndInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown xEndInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button drawButton;
        private OxyPlot.WindowsForms.PlotView rollPlotView;
        private OxyPlot.WindowsForms.PlotView accPlotView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown yawInput;
        private System.Windows.Forms.NumericUpDown hStartInput;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown hEndInput;
        private System.Windows.Forms.Label label6;
        private OxyPlot.WindowsForms.PlotView hPlotView;
    }
}

