﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstModel.Types
{
    public class TrackPoint
    {
        public double X { get; set; }
        public double Z { get; set; }
        public double H { get; set; }

        public TrackPoint(double x, double z)
        {
            this.X = x;
            this.Z = z;
            this.H = 0;
        }

        public TrackPoint(double x, double z, double h)
        {
            this.X = x;
            this.Z = z;
            this.H = h;
        }

        /// <summary>
        /// Возвращает расстояние между двумя точками на плоскости
        /// </summary>
        /// <param name="p1">Первая точка</param>
        /// <param name="p2">Вторая точка</param>
        /// <returns></returns>
        public static double GetDistance(TrackPoint p1, TrackPoint p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + 
                (p1.Z - p2.Z) * (p1.Z - p2.Z) +
                (p1.H - p2.H) * (p1.H - p2.H));
        }
    }
}
