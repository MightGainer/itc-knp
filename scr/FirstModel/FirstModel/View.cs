﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FirstModel.Models;
using FirstModel.Types;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace FirstModel
{
    public partial class View : Form
    {
        public View()
        {
            InitializeComponent();
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            double x0 = (double) this.xStartInput.Value;
            double z0 = (double) this.zStartInput.Value;
            double h0 = (double) this.hStartInput.Value;
            double xk = (double) this.xEndInput.Value;
            double zk = (double) this.zEndInput.Value;
            double hk = (double) this.hEndInput.Value;
            double yaw = (double) this.yawInput.Value;
            yaw = yaw * Math.PI / 180;

            SimpleModel model = new SimpleModel(
                new TrackPoint(x0, z0, h0),
                new TrackPoint(xk, zk, hk),
                yaw
                );

            model.FormControls();

            LineSeries routeSeries = model.GetRouteSeries();
            routeSeries.Title = "Путь";
            PlotModel coordinatesPlotModel = new PlotModel();

            // Adding axes
            double minVal = Math.Min((double)this.xStartInput.Value, (double)this.zStartInput.Value);
            double maxVal = Math.Max((double)this.xEndInput.Value, (double)this.zEndInput.Value);

            LinearAxis axisY = new LinearAxis();
            axisY.Minimum = minVal;
            axisY.Maximum = maxVal;
            axisY.Position = AxisPosition.Left;

            LinearAxis axisX = new LinearAxis();
            axisX.Minimum = minVal;
            axisX.Maximum = maxVal;
            axisX.Position = AxisPosition.Bottom;

            coordinatesPlotModel.Axes.Add(axisY);
            coordinatesPlotModel.Axes.Add(axisX);
            coordinatesPlotModel.Series.Add(routeSeries);
            this.coordinatesPlotView.Model = coordinatesPlotModel;

            LineSeries rollSeries = model.GetRollSeries();
            rollSeries.Title = "Roll";
            PlotModel rollPlotModel = new PlotModel();
            rollPlotModel.Series.Add(rollSeries);
            this.rollPlotView.Model = rollPlotModel;

            LineSeries accSeries = model.GetAccSeries();
            accSeries.Title = "Acc";
            PlotModel accPlotModel = new PlotModel();
            accPlotModel.Series.Add(accSeries);
            this.accPlotView.Model = accPlotModel;

            LineSeries heightSeries = model.GetHeightSeries();
            heightSeries.Title = "Height";
            PlotModel hPlotModel = new PlotModel();
            hPlotModel.Series.Add(heightSeries);
            this.hPlotView.Model = hPlotModel;
        }
    }
}
