﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstModel.Types;
using OxyPlot;
using OxyPlot.Series;

namespace FirstModel.Models
{
    /// <summary>
    /// Простая модель управляемого полета крылатой ракеты.
    /// Здесь принимаются следующие допущения:
    /// - ракета ведет свой полет на фиксированной высоте 150 метров
    /// - скорость ракеты постоянна и равна 500 метров в секунду
    /// - управление ведется за счет управления углом крена и вертикальной перегрузкой
    /// - угол тангажа не изменяется и равен 0 градусов
    /// </summary>
    class SimpleModel
    {
        #region Fields

        private const int numberOfDigits = 3;
        /// <summary>
        /// Ускорение свободного падения
        /// </summary>
        public const double g = 9.8;

        /// <summary>
        /// Скорость полета. Скорость постоянна
        /// </summary>
        private double speed { get; }

        /// <summary>
        /// Частота обновления управления
        /// </summary>
        private double controlFrequency { get; set; }

        /// <summary>
        /// Интервал обновления координат
        /// </summary>
        private double dt { get; set; }

        /// <summary>
        /// Начальная точка полета
        /// </summary>
        private TrackPoint startPoint { get; set; }

        /// <summary>
        /// Конечная точка полета
        /// </summary>
        private TrackPoint endPoint { get; set; }

        /// <summary>
        /// Маршрут
        /// </summary>
        private List<TrackPoint> route { get; set; }

        private double startYawAngle { get; set; }

        private double startPitchAngle { get; set; }

        private List<TrackPoint> rollControls { get; set; }

        private List<TrackPoint> accControls { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Базовый конструктор модели
        /// </summary>
        /// <param name="start">Стартовая точка полета</param>
        /// <param name="end">Конечная точка полета</param>
        /// <param name="yaw">Начальный угол курса</param>
        /// <param name="pitch">Начальный угол тангажа</param>
        /// <param name="controlTimeInterval">Интервал обновления управления</param>
        /// <param name="timeInterval">Интервал обновления данных</param>
        public SimpleModel(
            TrackPoint start,
            TrackPoint end,
            double yaw = 0,
            double pitch = 0,
            double timeInterval = 0.001
            )
        {
            this.speed = 500;

            this.startPoint = start;
            this.endPoint = end;

            this.controlFrequency = (TrackPoint.GetDistance(end, start) / speed);
            int tmp = (int)Math.Round(this.controlFrequency, numberOfDigits);

            this.controlFrequency = (double)tmp / 10;
            //double tmp2 = this.controlFrequency;
            //while (tmp2 > 1)
            //{
            //    tmp2 = tmp2 / 10;
            //}
            //this.controlFrequency = Math.Round(tmp2, numberOfDigits);
            this.dt = timeInterval;

            this.route = new List<TrackPoint>();

            this.startYawAngle = yaw;
            this.startPitchAngle = pitch;
            this.accControls = new List<TrackPoint>();
            this.rollControls = new List<TrackPoint>();

            //this.FormControls();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Функция подсчета координат
        /// </summary>
        /// <param name="startPoint">Начальная позиция</param>
        /// <param name="endPoint">Конечная позиция</param>
        /// <param name="speed">Имя</param>
        /// <param name="time">Время</param>
        /// <returns>координаты точки, где окажется ЛА.</returns>
        private static TrackPoint FindNext(
            TrackPoint startPoint,
            TrackPoint endPoint,
            double speed,
            double time
            )
        {
            double distance = TrackPoint.GetDistance(startPoint, endPoint);
            // Расстояние, пройденное за контрольный промежуток времени
            double d = speed * time;

            // Начальные данные
            double x0 = startPoint.X;
            double y0 = startPoint.Z;
            double h0 = startPoint.H;
            //double h0 = endPoint.H;
            double xk = endPoint.X;
            double yk = endPoint.Z;
            double hk = endPoint.H;

            double x, y, z;

            x = x0 + (d / distance) * (xk - x0);
            y = y0 + (d / distance) * (yk - y0);
            z = h0 + (d / distance) * (hk - h0);

            return new TrackPoint(x, y, z);
        }

        public double FormControls()
        {
            this.rollControls.Clear();
            this.accControls.Clear();
            this.route.Clear();

            // Счетчик времени
            double t = 0;
            
            // точность
            double accuracy = 10;
            
            // Текущая позиция
            TrackPoint pos = new TrackPoint(startPoint.X, startPoint.Z, startPoint.H);

            // Предыдущий угол курса
            double prevYaw = this.startYawAngle;
            double prevPitch = this.startPitchAngle;
            double newYaw, newX, newZ, newH, newPitch;

            // Константы
            double gdt = g * this.controlFrequency;
            double V_gdt = speed / gdt;

            // Элементы управления
            double rollControl = 0;
            double accControl = 0;

            double controlAccuracy = 1e-10;
            double timeLeftToControl = 0;

            while (TrackPoint.GetDistance(pos, endPoint) > accuracy)
            {
                // Обновляем контроль
                if (timeLeftToControl <= controlAccuracy)
                {
                    double controlTimeInterval = this.controlFrequency;
                    timeLeftToControl = this.controlFrequency;
                    /*
                     * Здесь может быть проблема выхода за границу, если controlFrequency будет слишком большой
                     */
                    TrackPoint nextPoint = SimpleModel.FindNext(pos, endPoint, speed, controlTimeInterval);
                    double neededYaw = Math.Acos(
                        (nextPoint.X - pos.X) /
                        (this.speed * controlTimeInterval * Math.Cos(Math.Asin(
                            (nextPoint.H - pos.H) / (this.speed * controlTimeInterval)
                            )))
                        );

                    double neededPitch = Math.Asin((nextPoint.H - pos.H) / (this.speed * controlTimeInterval));

                    double coef1 = (neededPitch - prevPitch) * V_gdt + Math.Cos(prevPitch);
                    rollControl = Math.Atan(
                        ((neededYaw - prevYaw) * V_gdt / Math.Cos(prevPitch)) / coef1
                        );
                    accControl = coef1 / Math.Cos(rollControl);
                }

                newYaw = g * Math.Cos(prevPitch) * accControl * Math.Sin(rollControl) * dt / speed + prevYaw;
                newPitch = dt * g / speed * (accControl * Math.Cos(rollControl) - Math.Cos(prevPitch)) + prevPitch;
                newX = pos.X + dt * speed * Math.Cos(newYaw) * Math.Cos(newPitch);
                newZ = pos.Z + dt * speed * Math.Sin(newYaw) * Math.Cos(newPitch);
                newH = pos.H + dt * speed * Math.Sin(newPitch);

                this.route.Add(new TrackPoint(pos.X, pos.Z, pos.H));
                rollControls.Add(new TrackPoint(t, rollControl));
                accControls.Add(new TrackPoint(t, accControl));

                prevYaw = newYaw;
                prevPitch = newPitch;
                pos.X = newX;
                pos.Z = newZ;
                pos.H = newH;

                t += dt;
                timeLeftToControl -= dt;
            }

            return t;
        }

        public LineSeries GetRouteSeries()
        {
            LineSeries result = new LineSeries();
            for (int i = 0; i < this.route.Count; i++)
            {
                result.Points.Add(new DataPoint(route[i].X, route[i].Z));
            }

            return result;
        }

        public LineSeries GetHeightSeries()
        {
            LineSeries result = new LineSeries();
            for (int i = 0; i < this.route.Count; i++)
            {
                result.Points.Add(new DataPoint(route[i].X, route[i].H));
            }

            return result;
        }

        public LineSeries GetRollSeries()
        {
            LineSeries result = new LineSeries();
            for (int i = 0; i < this.rollControls.Count; i++)
            {
                result.Points.Add(new DataPoint(rollControls[i].X, rollControls[i].Z));
            }

            return result;
        }

        public LineSeries GetAccSeries()
        {
            LineSeries result = new LineSeries();
            for (int i = 0; i < this.accControls.Count; i++)
            {
                result.Points.Add(new DataPoint(accControls[i].X, accControls[i].Z));
            }

            return result;
        }

        #endregion
    }
}
